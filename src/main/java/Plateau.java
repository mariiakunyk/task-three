public class Plateau {
	private int[] array;
	private int begin;
	private int end;
	private int length;
	
	public Plateau(int begin, int end, int[] array){
		this.array = array.clone();
		this.begin = begin;
		this.end = end;
		length = end - begin - 1;
	}
	
	public int[] getPlateau(){
		int[] plateau = new int[length + 2];
		int begin = this.begin;
		for(int i = 0; begin <= end; begin++, i++){
			plateau[i] = array[begin];
		}
		return plateau;
	}
	
	public int getLengthOfPlateau(){
		return length;
	}
	
	public int getBegin(){
		return begin + 1;
	}
	
	public int getEnd(){
		return end - 1;
	}
}
