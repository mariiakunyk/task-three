import java.util.Arrays;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {
        int[] array = {1,2,2,3,4,5,6,6,6,2};
        LongestPlateau l = new LongestPlateau(array);
        Iterator<Plateau> it = l.getLongestPlateau();
        while(it.hasNext()){
            Plateau plateau = it.next();
            System.out.print(Arrays.toString((plateau.getPlateau())));
            System.out.println(" : begin - " + plateau.getBegin() + ", end - " + plateau.getEnd());
        }
    }
}
