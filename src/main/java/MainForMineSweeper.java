
import java.util.Scanner;

    public class MainForMineSweeper {
        private static int m = 7;	//5 (7 - 2)
        private static int n = 7;	//5 (7 - 2) because borders
        private static double p = 0.2;
        private static Scanner sc = new Scanner(System.in);

        public MainForMineSweeper(int m1, int n1, double p1){
            m = m1;
            n = n1;
            p = p1;
        }

        public void start(){
            System.out.println("1 Start");
            System.out.println("2 Exit");
            System.out.print("Enter smth - ");
            int whatToDo = sc.nextInt();
            Action[] action = new Action[]{new Minesweeper(m, n, p), new Exit()};
            action[whatToDo - 1].doSomething(m, n, p);
        }


        public static void main(String[] args){
            MainForMineSweeper main = new MainForMineSweeper(MainForMineSweeper.m, MainForMineSweeper.n, MainForMineSweeper.p);
            main.start();
        }

        public class Exit implements Action{

            @Override
            public void doSomething(int m, int n, double p) {
                System.exit(0);
            }

        }
    }


