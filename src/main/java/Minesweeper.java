import java.util.Random;

public class Minesweeper implements Action{
	private Random random = new Random();
	private boolean[][] bombMatrix;
	private int[][] safeMatrix;
	private int p;

	public Minesweeper(int m, int n, double p){
		bombMatrix = new boolean[m][n];
		safeMatrix = new int[m][n];
		this.p = (int) (p * 1000);
	}

	public void createMinesweeper(){
		createMinesweeperBombs();
		createMinesweeperSafeSquares();
		printMinesweeper();
	}
	
	private void createMinesweeperBombs(){
		for(int i = 1; i < bombMatrix.length - 1; i++){
			for(int j = 1; j < bombMatrix[i].length - 1; j++){
				bombMatrix[i][j] = nextBoolean();
			}
		}		
	}
	
	private void createMinesweeperSafeSquares(){
		for(int i = 1; i < safeMatrix.length - 1; i++){
			for(int j = 1; j < safeMatrix[i].length - 1; j++){
				if(!bombMatrix[i][j]) safeMatrix[i][j] = getNumberOfNeighbourdBombs(i, j);
			}
		}		
	}
	
	private int getNumberOfNeighbourdBombs(int i, int j){
		int numberOfBombs = 0;
		for(int i1 = i - 1; i1 < i + 2; i1++){
			for(int j1 = j - 1; j1 < j + 2; j1++){
				try{
					if(bombMatrix[i1][j1]) numberOfBombs++;
				}catch(Exception e){}
			}
		}
		return numberOfBombs;
	}
	
	private void printMinesweeper(){
		for(int i = 0; i < bombMatrix.length; i++){
			for(int j = 0; j < bombMatrix[i].length; j++){
				if(i == 0 || i == bombMatrix.length - 1 || j == 0 || j == bombMatrix[i].length - 1) System.out.print("- ");
				else if(bombMatrix[i][j]) System.out.print("* ");
				else System.out.print(safeMatrix[i][j] + " ");
				if(j == bombMatrix[i].length - 1) System.out.println();
			}
		}	
	}

	private boolean nextBoolean(){
		return random.nextInt(1000) > p ? false : true;
	}

	@Override
	public void doSomething(int m, int n, double p) {
		Minesweeper minesweeper = new Minesweeper(m, n, p);
		minesweeper.createMinesweeper();
		
	}
}
