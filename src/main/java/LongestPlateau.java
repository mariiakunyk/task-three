import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LongestPlateau {
	private List<Plateau> plateau = new ArrayList<>();
	
	public LongestPlateau(int[] array){
		getPlateaus(array);
	}
	
	private void getPlateaus(int[] array){
		if(array.length < 3) throw new NullPointerException();
		for(int i = 1; i < array.length - 1; i++){
			if(array[i - 1] >= array[i]) 
				continue;
			else if(array[i] > array[i + 1])	//if length of plateau == 1
				plateau.add(new Plateau(i - 1, i + 1, array));
			else if(array[i] == array[i + 1]){	//if length of possible plateau > 1
				int begin = i - 1;
				while(array[i] == array[i + 1]){
					if(++i >= array.length - 1) return;
				}
				if(array[i] > array[i + 1]) plateau.add(new Plateau(begin, i + 1, array));
			}
		}
	}
	
	public Iterator<Plateau> getLongestPlateau(){
		return getLongestPlateaus(getLengthOfLongestPlateau()).iterator();
	}
	
	private List<Plateau> getLongestPlateaus(int length){
		List<Plateau> plateau = new ArrayList<>();
		Iterator<Plateau> it = this.plateau.iterator();
		while(it.hasNext()){
			Plateau temp = it.next();
			if(temp.getLengthOfPlateau() == length) plateau.add(temp);
		}
		return plateau;
	}
	
	private int getLengthOfLongestPlateau(){
		Iterator<Plateau> it = this.plateau.iterator();
		int maxLength = Integer.MIN_VALUE; 
		while(it.hasNext()){
			int temp = it.next().getLengthOfPlateau();
			if(temp > maxLength)
				maxLength = temp;
		}
		return maxLength;		
	}

}
